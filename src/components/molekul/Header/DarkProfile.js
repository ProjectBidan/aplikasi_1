import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {Button, Gap} from '../../atoms';

const DarkProfile = ({onPress, title, photo}) => {
  return (
    <View style={styles.container}>
      <Button type="icon-only" icon="back-light" onPress={onPress} />
      <View style={styles.content}>
        <Image source={photo} style={styles.avatar} />
        <Gap height={4} />
        <Text style={styles.name}>{title}</Text>
      </View>
    </View>
  );
};

export default DarkProfile;
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary2,
    paddingVertical: 10,
    paddingLeft: 16,
    paddingRight: 36,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  content: {
    flexDirection: 'column',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    width: 46,
    height: 46,
    borderRadius: 46 / 2,
  },
  name: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.white,
    textAlign: 'center',
  },
});
