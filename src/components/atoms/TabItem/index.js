import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {
  IconBidan,
  IconBidanActive,
  IconHospitals,
  IconHospitalsActive,
  IconMessages,
  IconMessagesActive,
} from '../../../assets';
import {colors, fonts} from '../../../utils';

const Tabitem = ({title, active, onPress, onLongPress}) => {
  const Icon = () => {
    if (title === 'Beranda') {
      return active ? <IconBidanActive /> : <IconBidan />;
    }
    if (title === 'Pesan') {
      return active ? <IconMessagesActive /> : <IconMessages />;
    }
    if (title === 'Rumah Sakit') {
      return active ? <IconHospitalsActive /> : <IconHospitals />;
    }
    return <IconBidan />;
  };
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={onPress}
      onLongPress={onLongPress}>
      <Icon />
      <Text style={styles.text(active)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Tabitem;
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  text: (active) => ({
    fontSize: 10,
    fontFamily: fonts.primary[600],
    color: active ? colors.text.menuactive : colors.text.menuInactive,
    marginTop: 4,
  }),
});
