import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyClvO6jYjrG7NJF4sEqopjT7ZHfKsStktU',
  authDomain: 'my-bidanku.firebaseapp.com',
  databaseURL: 'https://my-bidanku-default-rtdb.firebaseio.com',
  projectId: 'my-bidanku',
  storageBucket: 'my-bidanku.appspot.com',
  messagingSenderId: '154898328874',
  appId: '1:154898328874:web:6ad5be6c08870162c2c253',
});

const Fire = firebase;

export default Fire;
