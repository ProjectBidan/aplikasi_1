import DummyUser from './users.png';
import DummyNews1 from './news1.png';
import DummyNews2 from './news2.png';
import DummyNews3 from './news3.png';
import DummyPasien1 from './Pasien1.png';
import DummyPasien2 from './Pasien2.png';
import DummyPasien3 from './Pasien3.png';
import DummyHospital1 from './hospital1.png';
import DummyHospital2 from './hospital2.png';
import DummyHospital3 from './hospital3.png';
export {
  DummyUser,
  DummyNews1,
  DummyNews2,
  DummyNews3,
  DummyPasien1,
  DummyPasien2,
  DummyPasien3,
  DummyHospital1,
  DummyHospital2,
  DummyHospital3,
};
