import IconBackDark from './ic-back-dark.svg';
import IconBackLight from './ic-back-light.svg';
import IconAddPhoto from './ic-add-photo.svg';
import IconRemovePhoto from './ic-remove-photo.svg';
import IconBidan from './ic-bidan.svg';
import IconBidanActive from './ic-bidan-active.svg';
import IconMessages from './ic-messages.svg';
import IconMessagesActive from './ic-messages-active.svg';
import IconHospitals from './ic-hospitals.svg';
import IconHospitalsActive from './ic-hospitals-active.svg';
import IconDate from './ic-date.svg';
import IconSendDark from './ic-send-dark.svg';
import IconSendLight from './ic-send-light.svg';
import IconEditProfile from './ic-edit-profile.svg';
import IconHelp from './ic-help.svg';
export {
  IconBackDark,
  IconAddPhoto,
  IconRemovePhoto,
  IconBidan,
  IconBidanActive,
  IconMessages,
  IconMessagesActive,
  IconHospitals,
  IconHospitalsActive,
  IconDate,
  IconBackLight,
  IconSendDark,
  IconSendLight,
  IconEditProfile,
  IconHelp,
};
