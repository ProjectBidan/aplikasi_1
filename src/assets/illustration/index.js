import ILLogo from './logo.svg';
import ILLogoMini from './mini-logo.svg';
import ILLogoMedium from './logo-medium.svg';
import ILGetStarted from './background-getstarted.png';
import ILNullPhoto from './null-photo.png';
import ILHospitalBG from './hospital-background.png';
export {
  ILLogo,
  ILLogoMini,
  ILLogoMedium,
  ILGetStarted,
  ILNullPhoto,
  ILHospitalBG,
};
