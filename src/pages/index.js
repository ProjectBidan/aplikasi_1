import Splash from './Splash';
import GetStarted from './GetStarted';
import Register from './Register';
import Login from './Login';
import UploadPhoto from './UploadPhoto';
import Bidan from './Bidan';
import Messages from './Messages';
import Hospitals from './Hospitals';
import DetailNews from './DetailNews';
import Chatting from './Chatting';
import UserProfile from './UserProfile';
import UpdateProfile from './UpdateProfile';
export {
  Splash,
  GetStarted,
  Register,
  Login,
  UploadPhoto,
  Bidan,
  Messages,
  Hospitals,
  DetailNews,
  Chatting,
  UserProfile,
  UpdateProfile,
};
