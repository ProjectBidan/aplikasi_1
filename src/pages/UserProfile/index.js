import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Gap, Header, List, Profile} from '../../components';
import {colors, showError} from '../../utils';
import {Fire} from '../../config';
const UserProfile = ({navigation, route}) => {
  const profile = route.params;

  const signOut = () => {
    Fire.auth()
      .signOut()
      .then(() => {
        navigation.replace('GetStarted');
      })
      .catch((err) => {
        showError(err.message);
      });
  };
  return (
    <View style={styles.page}>
      <Header title="Profile" onPress={() => navigation.goBack()} />
      <Gap height={10} />
      {profile.fullName.length > 0 && (
        <Profile
          name={profile.fullName}
          desc={profile.profession}
          photo={profile.photo}
        />
      )}
      <Gap height={14} />
      <List
        name="Ubah Profil"
        desc="Terakhir update 24 januari 2021"
        type="next"
        icon="edit-proifle"
        onPress={() => navigation.navigate('UpdateProfile')}
      />
      <List
        name="Logout / Keluar"
        desc="Terakhir update 24 januari 2021"
        type="next"
        icon="help"
        onPress={signOut}
      />
    </View>
  );
};

export default UserProfile;
const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
});
