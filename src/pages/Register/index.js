import React, {useState} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {useDispatch} from 'react-redux';
import {Button, Gap, Header, Input} from '../../components';
import {Fire} from '../../config';
import {colors, showError, storeData, useForm} from '../../utils';

const Register = ({navigation}) => {
  const dispatch = useDispatch();
  const [form, setForm] = useForm({
    fullName: '',
    category: 'gading cempaka',
    university: '',
    str_number: '',
    hospital_address: '',
    gender: 'wanita',
    email: '',
    password: '',
  });

  const [itemCategory] = useState([
    {
      id: 1,
      label: 'Gading Cempaka',
      value: 'gading cempaka',
    },
    {
      id: 2,
      label: 'Kampung Melayu',
      value: 'kampung melayu',
    },
    {
      id: 3,
      label: 'Muara Bangkahulu',
      value: 'muara bangkahulu',
    },
    {
      id: 4,
      label: 'Ratu Agung',
      value: 'ratu agung',
    },
    {
      id: 5,
      label: 'Ratu Samban',
      value: 'ratu samban',
    },
  ]);

  const [itemGender] = useState([
    {
      id: 1,
      label: 'Wanita',
      value: 'wanita',
    },
    {
      id: 2,
      label: 'Pria',
      value: 'pria',
    },
  ]);
  const onContinue = () => {
    dispatch({type: 'SET_LOADING', value: true});
    Fire.auth()
      .createUserWithEmailAndPassword(form.email, form.password)
      .then((success) => {
        dispatch({type: 'SET_LOADING', value: false});
        setForm('reset');
        const data = {
          fullName: form.fullName,
          profession: form.category,
          category: form.category,
          university: form.university,
          str_number: form.str_number,
          hospital_address: form.hospital_address,
          gender: form.gender,
          email: form.email,
          uid: success.user.uid,
        };

        Fire.database().ref(`bidans/${success.user.uid}/`).set(data);

        storeData('user', data);
        navigation.navigate('UploadPhoto', data);
      })
      .catch((err) => {
        dispatch({type: 'SET_LOADING', value: false});
        showError(err.message);
      });
  };
  return (
    <ScrollView>
      <View style={styles.page}>
        <Header onPress={() => navigation.goBack()} title="Daftar Akun" />
        <View style={styles.content}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Input
              label="Nama Lengkap"
              value={form.fullName}
              onChangeText={(value) => setForm('fullName', value)}
            />
            <Gap height={24} />
            <Input
              label="Kecamatan(Wilayah Tempat Praktik)"
              value={form.category}
              onValueChange={(value) => setForm('category', value)}
              select
              selectItem={itemCategory}
            />
            <Gap height={24} />
            <Input
              label="Alamat Tempat Praktik"
              value={form.hospital_address}
              onChangeText={(value) => setForm('hospital_address', value)}
            />
            <Gap height={24} />
            <Input
              label="Universitas"
              value={form.university}
              onChangeText={(value) => setForm('university', value)}
            />
            <Gap height={24} />
            <Input
              label="No STR"
              value={form.str_number}
              onChangeText={(value) => setForm('str_number', value)}
            />
            <Gap height={24} />
            <Input
              label="Jenis Kelamin"
              value={form.gender}
              onValueChange={(value) => setForm('gender', value)}
              select
              selectItem={itemGender}
            />
            <Gap height={24} />
            <Input
              label="Email"
              value={form.email}
              onChangeText={(value) => setForm('email', value)}
            />
            <Gap height={24} />
            <Input
              label="Kata Sandi / Password"
              value={form.password}
              onChangeText={(value) => setForm('password', value)}
              secureTextEntry
            />
            <Gap height={40} />
            <Button title="Lanjutkan" onPress={onContinue} />
          </ScrollView>
        </View>
      </View>
    </ScrollView>
  );
};

export default Register;
const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  content: {
    padding: 40,
    paddingTop: 0,
  },
});
