import React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {IconDate} from '../../assets';
import {Gap, Header} from '../../components';
import {colors, fonts} from '../../utils';

const DetailNews = ({navigation, route}) => {
  const itemDetail = route.params;
  return (
    <View style={styles.page}>
      <Header onPress={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.content}>
          <Text style={styles.title}>{itemDetail.title}</Text>
          <Gap height={12} />
          <View style={styles.wrapperDate}>
            <IconDate />
            <Gap width={10} />
            <Text>{itemDetail.date}</Text>
          </View>
          <Gap height={13} />
          <Image source={{uri: itemDetail.image}} style={styles.image} />
          <Gap height={18} />
          <Text style={styles.description}>{itemDetail.body}</Text>
          <Text style={styles.description}>{itemDetail.body2}</Text>
        </View>
      </ScrollView>
    </View>
  );
};

export default DetailNews;
const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  content: {
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 18,
    fontFamily: fonts.primary[600],
    maxWidth: '90%',
    color: colors.text.primary,
  },
  wrapperDate: {
    flexDirection: 'row',
  },
  image: {
    width: 330,
    height: 187,
    borderRadius: 13,
  },
  description: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    textAlign: 'justify',
  },
});
