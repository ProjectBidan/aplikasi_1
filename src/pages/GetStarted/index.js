import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {ILGetStarted, ILLogoMini} from '../../assets';
import {Button, Gap} from '../../components';
import {colors, fonts} from '../../utils';

const GetStarted = ({navigation}) => {
  return (
    <ImageBackground source={ILGetStarted} style={styles.page}>
      <View style={styles.MiniHeader}>
        <Text style={styles.title}>
          Layani Konsultasi Pasien Jadi Lebih Mudah & Fleksibel
        </Text>
        <ILLogoMini />
      </View>
      <View style={styles.Button}>
        <Button
          title="Daftar Akun"
          onPress={() => navigation.navigate('Register')}
        />
        <Gap height={16} />
        <Button
          title="Masuk / Login"
          type="secondary"
          onPress={() => navigation.replace('Login')}
        />
      </View>
    </ImageBackground>
  );
};

export default GetStarted;
const styles = StyleSheet.create({
  page: {
    paddingVertical: 16,
    paddingHorizontal: 30,
    justifyContent: 'space-between',
    backgroundColor: colors.imagebackground,
    flex: 1,
  },
  title: {
    fontSize: 24,
    color: colors.secondary,
    fontWeight: '600',
    textAlign: 'left',
    maxWidth: 281,
    marginTop: 45,
    fontFamily: fonts.primary[600],
  },
  MiniHeader: {
    flexDirection: 'row',
  },
  Button: {
    paddingHorizontal: 10,
  },
});
