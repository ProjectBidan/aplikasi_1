import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ILLogo} from '../../assets';
import {colors, fonts} from '../../utils';
import {Fire} from '../../config';

const Splash = ({navigation}) => {
  useEffect(() => {
    const unsubscribe = Fire.auth().onAuthStateChanged((user) => {
      setTimeout(() => {
        if (user) {
          navigation.replace('MainApp');
        } else {
          navigation.replace('GetStarted');
        }
      }, 3000);
    });

    return () => unsubscribe();
  }, [navigation]);
  return (
    <View style={styles.page}>
      <ILLogo />
      <Text style={styles.title}>Bidan App</Text>
    </View>
  );
};

export default Splash;
const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    color: colors.text.tersier,
    marginTop: 20,
    fontFamily: fonts.primary[600],
  },
  page: {
    backgroundColor: colors.imagebackground,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
